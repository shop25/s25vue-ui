import { dtsImportsPlugin } from 'rollup-plugin-dts-imports'
import path from 'path'
import vue from 'rollup-plugin-vue'
import alias from '@rollup/plugin-alias'
import commonjs from '@rollup/plugin-commonjs'
import replace from '@rollup/plugin-replace'
// import { terser } from 'rollup-plugin-terser'
import globalStyles from 'rollup-plugin-vue-global-styles'
import peerDepsExternal from 'rollup-plugin-peer-deps-external'
import tsPlugin from 'rollup-plugin-typescript2'
import packageJson from './package.json'
import del from 'rollup-plugin-delete'
import babel from 'rollup-plugin-babel'
import copy from 'rollup-plugin-copy'

const projectRoot = path.resolve(__dirname)

export default {
  input: './ui/entry.ts',
  output: [
    {
      format: 'esm',
      file: packageJson.module,
      exports: 'named',
      inlineDynamicImports: true,
    },
  ],
  plugins: [
    del({ targets: 'dist/*' }),
    peerDepsExternal(),
    alias({
      resolve: ['.ts', '.vue', '.js', '.scss'],
      entries: {
        '@': path.resolve(projectRoot, 'ui'),
        '~quasar': 'node_modules/quasar',
      },
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
      'process.env.ES_BUILD': JSON.stringify('true'),
    }),
    tsPlugin({
      tsconfigOverride: {
        emitDeclarationOnly: true,
        exclude: ['dist', 'src', '*.json', '*.js', 'public', 'node_modules'],
      },
    }),
    dtsImportsPlugin({ aliasRoot: './ui' }),
    globalStyles({
      patterns: [
        './ui/css/variables/variables.sass',
        'node_modules/quasar/src/css/variables.sass',
      ],
    }),
    vue({
      css: true,
      template: {
        isProduction: true,
      },
    }),
    commonjs(),
    babel({
      exclude: 'node_modules/**',
      extensions: ['.js', '.jsx', '.ts', '.tsx', '.vue'],
      plugins: [
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        ['@babel/plugin-proposal-class-properties', { loose: true }],
      ],
      runtimeHelpers: true,
      presets: [['@babel/preset-env']],
    }),
    // terser(),
    copy({
      targets: [{ src: 'ui/css', dest: 'dist/assets' }],
    }),
  ],
}
