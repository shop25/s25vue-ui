export default interface Config {
  backendHost: string
  wsHost: string
}
