export interface TypeConfig {
  type: string
  innerEnabled: boolean
  outEnabled: boolean
}
