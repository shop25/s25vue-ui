import { NotificationCode } from '@/widgets/notifications/types/NotificationEvent'

export type TypesCount = Partial<
  {
    [key in NotificationCode]: TypeCount
  }
>

export type TypeCount = {
  count: number
  name: string
}
