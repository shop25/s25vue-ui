export interface NotificationEvent {
  id: number
  type: string
  group: string | null
  title: string
  text: string
  shortText: string
  createdAt: number
  actions: NotificationEventAction[]
  typeCode: NotificationCode
}

export interface NotificationEventAction {
  label: string
  link: string
}

export enum NotificationCode {
  newOrder = 'newOrder',
  newPayment = 'newPayment',
  newTask = 'newTask',
  scheduledTask = 'scheduledTask',
  taskComplete = 'taskComplete',
  newMessage = 'newMessage',
  other = 'other',
}
