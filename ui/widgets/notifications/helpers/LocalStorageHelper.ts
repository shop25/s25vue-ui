enum storageKeys {
  notificationsCount,
  notificationDuration,
  excludedTypes,
  popupTypes,
}

class LocalStorageHelper {
  clientUid: null | string = null

  init(payload: { clientUid: string }) {
    this.clientUid = payload.clientUid
  }

  saveNotificationsCount(value: number) {
    this.localSaveValue(value, storageKeys.notificationsCount)
  }

  getNotificationsCount(): number | null {
    return this.getLocalStorageValue<number>(storageKeys.notificationsCount)
  }

  saveDuration(value: number) {
    this.localSaveValue(value, storageKeys.notificationDuration)
  }

  getDuration(): number | null {
    return this.getLocalStorageValue<number>(storageKeys.notificationDuration)
  }

  saveExcludedTypes(types: string[]) {
    this.localSaveValue(types, storageKeys.excludedTypes)
  }

  getExcludedTypes(): string[] {
    return this.getLocalStorageValue<string[]>(storageKeys.excludedTypes) || []
  }

  saveExcludedPopupTypes(types: string[]) {
    this.localSaveValue(types, storageKeys.popupTypes)
  }

  getExcludedPopupTypes(): string[] {
    return this.getLocalStorageValue<string[]>(storageKeys.popupTypes) || []
  }

  private getLocalStorageKey(key: number) {
    return `s25ui-notification_${this.clientUid}${key}`
  }

  private localSaveValue<T>(value: T, storageKey: number) {
    localStorage.setItem(
      this.getLocalStorageKey(storageKey),
      JSON.stringify(value)
    )
  }

  private getLocalStorageValue<T>(key: number): T | null {
    const localData = localStorage.getItem(this.getLocalStorageKey(key))
    return localData ? (JSON.parse(localData) as T) : null
  }
}

export default new LocalStorageHelper()
