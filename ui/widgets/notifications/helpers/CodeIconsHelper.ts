import { NotificationCode } from '@/widgets/notifications/types/NotificationEvent'

class CodeIconsHelper {
  icons: { [key in NotificationCode]: string } = {
    [NotificationCode.newMessage]: 'la la-envelope',
    [NotificationCode.newOrder]: 'la la-store',
    [NotificationCode.newPayment]: 'la la-coins',
    [NotificationCode.newTask]: 'la la-clipboard-check',
    [NotificationCode.taskComplete]: 'la la-clipboard-check',
    [NotificationCode.scheduledTask]: 'la la-clipboard-check',
    [NotificationCode.other]: 'la la-question-circle',
  }

  getIcon(code: NotificationCode): string {
    return this.icons[code]
  }
}

export default new CodeIconsHelper()
