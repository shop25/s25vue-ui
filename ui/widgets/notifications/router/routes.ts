import { RouteConfig } from 'vue-router'
import { RouteNames } from '@/widgets/notifications/router/routeNames'
import NotificationsLayout from '@/widgets/notifications/layouts/NotificationsLayout.vue'
import IndexPage from '@/widgets/notifications/pages/IndexPage.vue'
import ConfigPage from '@/widgets/notifications/pages/ConfigPage.vue'

export const Routes: RouteConfig[] = [
  {
    path: 'widgets/notifications',
    component: NotificationsLayout,
    children: [
      {
        name: RouteNames.config,
        path: 'config',
        component: ConfigPage,
      },
      {
        name: RouteNames.index,
        path: ':codes?',
        props: true,
        component: IndexPage,
      },
    ],
  },
]
