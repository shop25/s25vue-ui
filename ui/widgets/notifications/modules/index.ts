import NotificationsModule from './NotificationsModule'
import NotificationsConfigModule from './NotificationsConfigModule'

export const NotificationsModules = {
  NotificationsModule,
  NotificationsConfigModule,
}

export { NotificationsModule, NotificationsConfigModule }
