import { VuexModule, Module, Mutation } from 'vuex-module-decorators'
import LocalStorageHelper from '../helpers/LocalStorageHelper'

@Module({
  name: 'NotificationsConfigModule',
  namespaced: true,
})
export default class NotificationsConfigModule extends VuexModule {
  notificationsCount = LocalStorageHelper.getNotificationsCount() || 5
  notificationDuration = LocalStorageHelper.getDuration() || 20

  @Mutation
  setNotificationCount(value: number) {
    this.notificationsCount = value
    LocalStorageHelper.saveNotificationsCount(value)
  }

  @Mutation
  setNotificationDuration(value: number) {
    this.notificationDuration = value
    LocalStorageHelper.saveDuration(value)
  }
}
