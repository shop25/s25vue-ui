import axios from 'axios'
import NotificationWebsocketService from '@/widgets/notifications/http/services/NotificationWebsocketService'

export const httpClient = axios.create()

httpClient.defaults.withCredentials = true
httpClient.defaults.baseURL = process.env.NOTIFICATIONS_API_URL

httpClient.interceptors.request.use(
  function(config) {
    config.headers['X-Site'] = process.env.PROJECT_NAME

    if (NotificationWebsocketService.client) {
      config.headers[
        'X-Socket-Id'
      ] = NotificationWebsocketService.client.socketId()
    }

    return config
  },
  function(error) {
    return Promise.reject(error)
  }
)
