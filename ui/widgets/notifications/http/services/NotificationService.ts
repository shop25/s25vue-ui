import {
  NotificationCode,
  NotificationEvent,
} from '@/widgets/notifications/types/NotificationEvent'
import { httpClient } from '@/widgets/notifications/http/HttpClient'
import { TypesCount } from '@/widgets/notifications/types/TypesCount'

interface GetNotificationPayload {
  count?: number
  codes?: NotificationCode[]
  excludedCodes?: string[]
}

class NotificationService {
  getNotifications(
    payload?: GetNotificationPayload
  ): Promise<{ notifications: NotificationEvent[]; count: number }> {
    const params: GetNotificationPayload = {
      count: payload?.count,
      codes: payload?.codes,
    }

    if (!payload?.codes || !payload?.codes?.length) {
      params.excludedCodes = payload?.excludedCodes
    }

    return httpClient
      .get('notifications', { params })
      .then(response => response.data)
  }

  loadTypes(): Promise<TypesCount> {
    return httpClient.get('types').then(response => response.data.types)
  }

  closeNotification(notificationId: number) {
    return httpClient.post(`notifications/close/${notificationId}`)
  }

  closeAllNotifications() {
    return httpClient.post('notifications/close')
  }
}

export default new NotificationService()
