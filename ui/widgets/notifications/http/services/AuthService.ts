import { BaseAuthService } from '@/services'

class AuthService extends BaseAuthService {
  baseUrl = process.env.NOTIFICATIONS_API_URL || ''
}

export default new AuthService()
