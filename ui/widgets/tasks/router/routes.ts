import { RouteConfig } from 'vue-router'
import { RouteNames } from '@/widgets/tasks/router/routeNames'
import TasksLayout from '@/widgets/tasks/layouts/TasksLayout.vue'
import IndexPage from '@/widgets/tasks/pages/IndexPage.vue'
import OwnTasksPage from '@/widgets/tasks/pages/OwnTasksPage.vue'
import FinishedTasksPage from '@/widgets/tasks/pages/FinishedTasksPage.vue'
import CategoryTasksPage from '@/widgets/tasks/pages/CategoryTasksPage.vue'
import OrderTasksPage from '@/widgets/tasks/pages/OrderTasksPage.vue'
import CategoriesPage from '@/widgets/tasks/pages/CategoriesPage.vue'
import SearchResultPage from '@/widgets/tasks/pages/SearchResultPage.vue'
import CreateTaskPage from '@/widgets/tasks/pages/CreateTaskPage.vue'

export const Routes: RouteConfig[] = [
  {
    path: 'widgets/task-book',
    component: TasksLayout,
    children: [
      {
        path: 'tasks',
        name: RouteNames.tasks,
        component: IndexPage,
      },
      {
        path: 'tasks/own',
        name: RouteNames.ownTasks,
        component: OwnTasksPage,
      },
      {
        path: 'tasks/finished',
        name: RouteNames.finishedTasks,
        component: FinishedTasksPage,
      },
      {
        path: 'tasks/:group/:categoryId?',
        name: RouteNames.categoryTasks,
        props: true,
        component: CategoryTasksPage,
      },
      {
        path: 'order/:orderName',
        name: RouteNames.orderTasks,
        props: true,
        component: OrderTasksPage,
      },
      {
        path: 'categories',
        name: RouteNames.categories,
        component: CategoriesPage,
      },
      {
        path: 'search',
        name: RouteNames.search,
        component: SearchResultPage,
      },
      {
        path: 'create',
        name: RouteNames.createTask,
        component: CreateTaskPage,
      },
    ],
  },
]
