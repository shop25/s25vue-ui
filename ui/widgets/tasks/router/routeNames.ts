export enum RouteNames {
  createTask = 'tasks/create',
  tasks = 'tasks',
  ownTasks = 'tasks/own',
  finishedTasks = 'tasks/finished',
  categoryTasks = 'tasks/category',
  orderTasks = 'tasks/order',
  categories = 'tasks/categories',
  search = 'tasks/search',
}
