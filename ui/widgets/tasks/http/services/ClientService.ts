import SearchedClient from '@/widgets/base/types/SearchedClient'
import Client from '@/widgets/base/types/Client'
import { httpClient } from '@/widgets/tasks/http/HttpClient'
import { Order } from '@/widgets/base/types/Order'

class ClientService {
  searchClients(query: string): Promise<SearchedClient[]> {
    return httpClient
      .get(`client/search?searchQuery=${query}`)
      .then(response => response.data)
  }

  getClientById(clientId: number): Promise<Client> {
    return httpClient
      .get(`client/search-by-id/${clientId}`)
      .then(response => response.data.client)
  }

  getClientByOrderName(orderName: string): Promise<Client> {
    return httpClient
      .get(`client/search-by-order-name?searchQuery=${orderName}`)
      .then(response => response.data.client)
  }

  getOrdersByClientId(clientId: number): Promise<Order[]> {
    return httpClient
      .get(`client/orders-by-client-id/${clientId}`)
      .then(response => response.data)
  }
}

export default new ClientService()
