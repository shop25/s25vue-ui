import { httpClient } from '@/widgets/tasks/http/HttpClient'
import { ManagerCollectionsByGroup } from '@/types/Manager'

class ManagerService {
  loadManagers(): Promise<ManagerCollectionsByGroup> {
    return httpClient
      .get<ManagerCollectionsByGroup>(
        `api/users?project=${process.env.PROJECT_NAME}`
      )
      .then(response => response.data)
  }
}

export default new ManagerService()
