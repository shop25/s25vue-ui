import { Category, CategoryGroupCounting } from '@/widgets/tasks/types/Category'
import { httpClient } from '@/widgets/tasks/http/HttpClient'

class CategoryService {
  loadCategories(): Promise<Category[]> {
    return httpClient.get('category/list').then(response => response.data)
  }

  loadCounting(): Promise<CategoryGroupCounting> {
    return httpClient
      .get('category/group-count')
      .then(response => response.data)
  }
}

export default new CategoryService()
