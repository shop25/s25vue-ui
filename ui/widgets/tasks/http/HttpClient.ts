import axios from 'axios'
import WebsocketService from '@/widgets/tasks/http/services/WebsocketService'

const httpClient = axios.create({
  baseURL: process.env.TASKS_API_URL,
  withCredentials: true,
})

httpClient.interceptors.request.use(
  function(config) {
    config.headers['X-Site'] = process.env.PROJECT_NAME

    if (WebsocketService.client) {
      config.headers['X-Socket-Id'] = WebsocketService.client.socketId()
    }

    return config
  },
  function(error) {
    return Promise.reject(error)
  }
)

export { httpClient }
