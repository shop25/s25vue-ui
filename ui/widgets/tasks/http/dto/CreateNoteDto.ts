import { TaskType } from '@/widgets/tasks/types/Task'

export interface CreateNoteDto {
  text: string
  files: File[]
  type: TaskType.note
}
