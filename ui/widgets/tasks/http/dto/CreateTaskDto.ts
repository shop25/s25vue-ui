import { TaskType } from '@/widgets/tasks/types/Task'

export interface CreateTaskDto {
  categoryId: number | null
  candidateUids: string[]
  clientId: number | null
  orderName: string | null
  text: string
  executeUntil: number
  level: number
  files: File[]
  type: TaskType.task
}
