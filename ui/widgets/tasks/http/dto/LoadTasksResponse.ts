import { Task } from '@/widgets/tasks/types/Task'

export type LoadTasksResponse = {
  tasks: Task[]
  totalCount: number
}
