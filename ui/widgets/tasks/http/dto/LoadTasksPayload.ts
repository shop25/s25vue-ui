import { TaskFilter } from '@/widgets/tasks/types/TaskFilter'

export type LoadTasksPayload = {
  ids?: number[]
  categoryId?: number | null
  orderName?: string | null
  dateGroup?: string | null
  searchQuery?: string
  status?: TaskFilter
}
