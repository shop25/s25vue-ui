import { Task } from '@/widgets/tasks/types/Task'

export interface UpdateTaskDto extends Task {
  files?: File[]
}
