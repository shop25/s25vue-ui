export interface AddCommentDto {
  taskId: number
  comment: string
}
