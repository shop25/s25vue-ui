import { CategoryModule } from '@/widgets/tasks/modules/CategoryModule'
import { ManagerModule } from '@/widgets/tasks/modules/ManagerModule'
import { TaskModule } from '@/widgets/tasks/modules/TaskModule'
import { TaskUserModule } from '@/widgets/tasks/modules/TaskUserModule'

export const TasksModules = {
  [ManagerModule.storeName]: ManagerModule,
  CategoryModule,
  TaskModule,
  TaskUserModule,
}

export { CategoryModule, TaskModule, TaskUserModule }
