import { Action, Module } from 'vuex-module-decorators'
import { BaseUserModule } from '@/store/BaseUserModule'
import { UserService } from '@/services/UserService'
import { NotifyHelper } from '@/helpers'
import { httpClient } from '@/widgets/messages/services/HttpClient'

const userService = new UserService(httpClient)

@Module({
  name: 'TaskUserModule',
  namespaced: true,
})
export class TaskUserModule extends BaseUserModule {
  @Action
  async loadUser() {
    if (this.user) {
      return
    }

    try {
      this.setLoading(true)

      this.setUser(await userService.loadUser())
    } catch (e) {
      NotifyHelper.error('Вы не авторизованы')
    } finally {
      this.setLoading(false)
    }
  }

  @Action
  async logout() {
    try {
      await userService.logout()

      this.setUser(null)
    } catch (e) {
      NotifyHelper.serverError()
    }
  }
}
