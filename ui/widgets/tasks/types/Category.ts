export type Category = {
  id: number
  title: string
  color: string
  isPublished: boolean
}

export type CategoryGroup = string

export type CategoryGroupCounting = {
  [key in CategoryGroup]: {
    title: string
    groupCount: CategoryCounting[]
    allCount: number
  }
}

export type CategoryCounting = {
  id: number
  taskCount: number
}
