import { AttachmentFile } from '@/types'
import { TaskComment } from '@/widgets/tasks/types/TaskComment'

export type Task = {
  id: number
  creatorUid: string
  performerUid: string | null
  candidateUids: string[]
  categoryId: number
  dateGroup: string
  clientId: number | null
  level: number
  text: string
  type: TaskType
  orderName: string
  orderDate: number | null
  clientName: string
  clientPhone: string | null
  executeUntil: number
  createdAt: number
  updatedAt: number
  finishedAt: null | number
  attachments: AttachmentFile[]
  comments: TaskComment[]
}

export enum TaskType {
  task,
  note,
}
