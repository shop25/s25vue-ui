export type TaskComment = {
  id: number
  taskId: number
  creatorUid: string
  text: string
  createdAt: number
}
