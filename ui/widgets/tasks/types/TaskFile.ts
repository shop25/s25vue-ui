export type TaskFile = {
  id: number
  taskId: number
  fileName: string
  title: string
  extension: string
  createdAt: number
}
