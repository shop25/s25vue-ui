import { Task } from '@/widgets/tasks/types/Task'
import { Manager } from '@/types/Manager'

class TaskEditHelper {
  isEditAllowed(task: Task, manager: Manager): boolean {
    return [task.performerUid, task.creatorUid].includes(manager.id)
  }
}

export default new TaskEditHelper()
