class TaskLevelHelper {
  levelColors: Map<number, string> = new Map([
    [-3, 'bg-blue-grey-4'],
    [-2, 'bg-blue-grey-3'],
    [-1, 'bg-blue-grey-2'],
    [0, 'bg-background text-blue-grey-3'],
    [1, 'bg-red-2'],
    [2, 'bg-red-4'],
    [3, 'bg-negative'],
  ])

  getColor(level: number): string {
    return this.levelColors.get(level) as string
  }

  getLevels(): number[] {
    return [...this.levelColors.keys()].reverse()
  }
}

export default new TaskLevelHelper()
