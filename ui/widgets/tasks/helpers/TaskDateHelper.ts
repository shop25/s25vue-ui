import { Task, TaskType } from '@/widgets/tasks/types/Task'
import DateHelper from '@/helpers/DateHelper'

export function getPrettyTime(
  task: Task,
  isSearch: boolean,
  isUntil: boolean
): string {
  // не показываем
  // если заметка
  if (task.type === TaskType.note) {
    return ''
  }

  if (isUntil) {
    return DateHelper.format(task.executeUntil)
  }

  // Если это не поиск
  // показываем только время
  const time = DateHelper.getHumanizeTime(task.executeUntil)

  if (!isSearch) {
    return time
  }

  // если это поиск
  // 1. если не прошедшее время то добавляем префикс сегодня, завтра
  // 2. инача DateHelper.format
  const currentDate = new Date()
  const untilDate = DateHelper.getDateFromTimestamp(task.executeUntil)
  const isExpired = untilDate.getTime() < currentDate.getTime()
  const isToday = untilDate.getDay() === currentDate.getDay()

  return isExpired
    ? DateHelper.format(task.executeUntil)
    : `${isToday ? 'Сегодна' : 'Завтра'} ${time}`
}
