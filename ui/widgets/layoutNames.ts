export const layoutNames = ['messages', 'tasks', 'notifications'] as const

export type LayoutName = typeof layoutNames[number]
