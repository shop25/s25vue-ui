import Translit from 'cyrillic-to-translit-js'
import { ru } from 'convert-layout'

const transilte = new Translit({ preset: 'ru' })

export const getQueryVariants = (query: string): string[] => {
  query = query.toLowerCase().trim()

  return [
    query,
    transilte.transform(query),
    transilte.reverse(query),
    ru.fromEn(query),
    ru.toEn(query),
    transilte.reverse(ru.toEn(query)),
  ]
}
