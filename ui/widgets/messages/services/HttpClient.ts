import MessageWebsocketService from '@/widgets/messages/services/MessageWebsocketService'
import { HttpClientFactory } from '@/widgets/base/http/HttpClientFactory'
import AuthService from '@/widgets/messages/services/AuthService'

export const httpClient = HttpClientFactory({
  config: { withCredentials: true, baseURL: process.env.MESSAGES_API_URL },
  authenticate: () => AuthService.getAuthToken(),
  getSocketId: () =>
    MessageWebsocketService.client
      ? MessageWebsocketService.client.socketId()
      : '',
})
