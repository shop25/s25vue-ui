import { httpClient } from '@/widgets/messages/services/HttpClient'
import { Order } from '@/widgets/base/types/Order'
import SearchedClient from '@/widgets/base/types/SearchedClient'
import Client from '@/widgets/base/types/Client'

class ClientService {
  searchClients(query: string): Promise<SearchedClient[]> {
    return httpClient
      .get(`clients?query=${query}`)
      .then(response => response.data)
  }

  getClientById(clientId: number): Promise<Client> {
    return httpClient
      .get(`clients/${clientId}`)
      .then(response => response.data.client)
  }

  getClientByOrderName(orderName: string): Promise<Client> {
    return httpClient
      .get(`clients/search-by-order-name?searchQuery=${orderName}`)
      .then(response => response.data.client)
  }

  getOrdersByClientId(clientId: number): Promise<Order[]> {
    return httpClient
      .get(`/clients/${clientId}/orders`)
      .then(response => response.data)
  }
}

export default new ClientService()
