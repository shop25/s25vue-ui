import { BaseAuthService } from '@/services'

class AuthService extends BaseAuthService {
  baseUrl = process.env.MESSAGES_API_URL || ''
}

export default new AuthService()
