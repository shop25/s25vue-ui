import { httpClient } from '@/widgets/messages/services/HttpClient'
import { MessageFilterType } from '@/widgets/messages/types/CountingMessages'

class CountingMessagesService {
  getCounting(type: MessageFilterType): Promise<number> {
    return httpClient
      .get<number>(`/counts?type=${type}`, { withCredentials: true })
      .then(response => response.data)
  }
}

export default new CountingMessagesService()
