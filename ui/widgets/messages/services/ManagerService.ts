import { httpClient } from '@/widgets/messages/services/HttpClient'
import { ManagerCollectionsByGroup } from '@/types/Manager'

class ManagerService {
  getManagers(): Promise<ManagerCollectionsByGroup> {
    return httpClient
      .get<ManagerCollectionsByGroup>(
        `api/users?project=${process.env.PROJECT_NAME}`
      )
      .then(response => response.data)
  }
}

export default new ManagerService()
