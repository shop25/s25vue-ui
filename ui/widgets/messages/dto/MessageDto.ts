import { Message, MessageType } from '@/widgets/messages/types/Message'

interface BaseMessageDto {
  text: string
  type: MessageType
}

export interface CreateMessageDto extends BaseMessageDto {
  managerUids: string[]
  orderName: string | null
  clientId: number | null
  type: MessageType
  files?: File[]
}

export interface ReplyMessageDto extends BaseMessageDto {
  type: MessageType
  files: File[]
  threadId: number
}

export interface UpdateMessageDto extends Message {
  files?: File[]
}

export interface SearchMessagesDto {
  query: string
  ids: number[]
}
