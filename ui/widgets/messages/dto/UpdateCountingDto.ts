import { MessageFilterType } from '@/widgets/messages/types/CountingMessages'

export type UpdateCountingDto = { type: MessageFilterType; count: number }
