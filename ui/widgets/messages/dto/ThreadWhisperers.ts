export interface ThreadWhisperers {
  threadId: number
  managerUids: string[]
}
