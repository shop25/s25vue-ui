import CountingMessagesModule from './CountingMessagesModule'
import MessageModule from './MessageModule'
import { ManagerModule } from './ManagerModule'
import { MessageUserModule } from './MessageUserModule'

export const MessagesModules = {
  [ManagerModule.storeName]: ManagerModule,
  CountingMessagesModule,
  MessageModule,
  MessageUserModule,
}

export { CountingMessagesModule, MessageModule, MessageUserModule }
