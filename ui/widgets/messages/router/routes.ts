import { RouteConfig } from 'vue-router'
import { RouteNames } from '@/widgets/messages/router/routeNames'
import IndexPage from '@/widgets/messages/pages/IndexPage.vue'
import SearchPage from '@/widgets/messages/pages/SearchPage.vue'
import OrderThreadsPage from '@/widgets/messages/pages/OrderThreadsPage.vue'
import CreateThreadPage from '@/widgets/messages/pages/CreateThreadPage.vue'
import ThreadPage from '@/widgets/messages/pages/ThreadPage.vue'
import MessagesLayout from '@/widgets/messages/layouts/MessagesLayout.vue'

export const Routes: RouteConfig[] = [
  {
    path: 'widgets/messages',
    component: MessagesLayout,
    children: [
      {
        name: RouteNames.search,
        path: 'search/:query?',
        component: SearchPage,
      },
      {
        name: RouteNames.orderThreads,
        path: 'order/:orderName',
        props: true,
        component: OrderThreadsPage,
      },
      {
        name: RouteNames.create,
        path: 'create',
        component: CreateThreadPage,
      },
      {
        name: RouteNames.thread,
        path: 'thread/:threadId',
        props: true,
        component: ThreadPage,
      },
      {
        name: RouteNames.index,
        path: ':type?',
        component: IndexPage,
      },
    ],
  },
]
