export enum RouteNames {
  index = 'messages/index',
  thread = 'messages/thread',
  create = 'messages/create',
  search = 'messages/search',
  orderThreads = 'messages/orderThreads',
}
