import { HashNavEntry } from '@/widgets/base/types/HashNavEntry'
import { buildHashNavRoutes } from '@/widgets/base/hashNavHelper'
import { PageName } from '@/widgets/messages/router/pageNames'

type Entry = HashNavEntry<PageName>

const entries: Omit<Entry, 'layoutName'>[] = [
  {
    pageName: 'search' as const,
    path: '#/widgets/messages/search/:query?',
  },

  {
    pageName: 'orderThreads' as const,
    path: '#/widgets/messages/order/:orderName',
  },

  {
    pageName: 'createThread' as const,
    path: '#/widgets/messages/create',
  },

  {
    pageName: 'thread' as const,
    path: '#/widgets/messages/thread/:threadId',
  },

  {
    pageName: 'index' as const,
    path: '#/widgets/messages/:type?',
  },
]

export const hashNavRoutes = buildHashNavRoutes(
  entries.map(e => ({ ...e, layoutName: 'messages' as const }))
)
