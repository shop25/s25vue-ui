export const pageNames = [
  'search',
  'orderThreads',
  'createThread',
  'thread',
  'index',
] as const

export type PageName = typeof pageNames[number]
