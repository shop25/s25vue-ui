export enum MessageFilterType {
  openedThreads = 'opened',
  favoriteThreads = 'favorite',
  subscribedThread = 'subscribed',
}

export type CountingMessages = Partial<
  {
    [key in MessageFilterType]: number
  }
>
