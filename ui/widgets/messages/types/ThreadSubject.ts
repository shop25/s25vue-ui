export interface ThreadSubject {
  id: number
  label: string
}
