import { AttachmentFile } from '@/types'
import Client from '@/widgets/base/types/Client'

export enum MessageType {
  sms = 'sms',
  feedback = 'feedback',
}

export enum SenderType {
  client = 'client',
  manager = 'manager',
}

export interface Message {
  id: number
  body: string
  type: MessageType
  senderType: SenderType

  // Привязки
  threadId: number
  senderId: string
  deleterId: number | null
  isRead: boolean

  // даты
  deletedAt: number | null
  createdAt: number
  updatedAt: number | null
  attachments: AttachmentFile[]
  // smsNotification: NotificationEvent | null
  // mailNotification: NotificationEvent | null
}

export interface FoundMessage extends Message {
  client: Client | null
}
