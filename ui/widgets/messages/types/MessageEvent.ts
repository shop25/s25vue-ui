// Статус pending говорит о том, что ожидается отправка.
// Поле processedAt в данном случае задается датой, когда будет совершена отправка.
export type NotificationEventStatus =
  | 'sent'
  | 'received'
  | 'read'
  | 'error'
  | 'pending'

export default interface MessageEvent {
  messageId: number
  status: NotificationEventStatus
  processedAt: number
}
