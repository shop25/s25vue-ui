import { Message } from '@/widgets/messages/types/Message'

export type PreviewMessages = {
  first: Message[]
  last: Message[]
}
