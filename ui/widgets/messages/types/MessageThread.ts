import { Message } from './Message'
import Client from '@/widgets/base/types/Client'

export interface MessageThread {
  id: number
  clientId: number
  processedManagerUid: string | null
  isFavorite: boolean
  subjectId: number

  createdAt: number
  updatedAt: number
  processedAt: number | null

  messages: Message[]
  managerUids: string[]

  client: Client
  refObject: RefObject | null
}

export type RefObject = {
  id: string
  title: string
  url: string
  type: RefObjectType
}

export enum RefObjectType {
  order = 'order',
}
