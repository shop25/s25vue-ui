import { Component, Emit, Inject, Vue } from 'vue-property-decorator'
import { MessageType } from '@/widgets/messages/types/Message'
import { ResponseErrors } from '@/types'
import MessageModule from '@/widgets/messages/modules/MessageModule'
import ClientService from '@/widgets/messages/services/ClientService'

@Component
export default class CommonMessageForm extends Vue {
  @Inject('MessageModule')
  messageModule!: MessageModule

  types = {
    [MessageType.feedback]: 'Сообщение',
    [MessageType.sms]: 'SMS',
  }

  errors: ResponseErrors = {
    text: '',
    clientId: '',
    orderName: '',
  }

  isLoading = false

  files: File[] = []

  get messageTypes() {
    return MessageType
  }

  get searchClientFn() {
    return ClientService.searchClients
  }

  get getClientById() {
    return ClientService.getClientById
  }

  get getClientByOrderName() {
    return ClientService.getClientByOrderName
  }

  get getOrdersByClientId() {
    return ClientService.getOrdersByClientId
  }

  selectFiles(files: File[]) {
    this.files = [...this.files, ...files]
  }

  @Emit()
  save() {
    return true
  }

  @Emit()
  cancel() {
    return true
  }
}
