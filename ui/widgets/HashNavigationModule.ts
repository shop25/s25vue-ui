import { Module, Mutation, VuexModule } from 'vuex-module-decorators'

interface Params {
  layoutName: string | null
  pageName: string | null
  props: Record<string, string>
}

@Module({ name: 'HashNavigationModule', namespaced: true })
export class HashNavigationModule extends VuexModule {
  layoutName: string | null = null
  pageName: string | null = null
  props: Record<string, string> = {}

  @Mutation
  setParams({ layoutName, pageName, props }: Params) {
    this.layoutName = layoutName
    this.pageName = pageName
    this.props = props
  }
}
