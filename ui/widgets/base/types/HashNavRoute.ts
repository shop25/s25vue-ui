export interface HashNavRoute<C extends string> {
  layoutName: C
  pageName: C
  match: (hash: string) => false | Record<string, string>
  compile: (params?: Record<string, string>) => string
}
