export interface HashNavEntry<C extends string> {
  path: string
  layoutName: C
  pageName: C
}
