import Client from '@/widgets/base/types/Client'

export default interface SearchedClient extends Client {
  matched: string
  matchedByName: boolean
}
