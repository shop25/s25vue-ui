export default interface Client {
  id: number
  email: string
  phone: string
  fullName: string | null
  avatar: string | null
}
