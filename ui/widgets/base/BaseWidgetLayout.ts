import { Component, Vue } from 'vue-property-decorator'
import { RouteHelper } from '@/widgets/base/RouteHelper'

@Component({
  beforeRouteEnter(to, from, next) {
    // const lastWidgetLocation = RouteHelper.getWidgetLocation(to)
    // RouteHelper.saveLastLocation(to)
    //
    // if (lastWidgetLocation && lastWidgetLocation.path !== to.path) {
    //   next({ path: lastWidgetLocation.path })
    // } else {
    //   next()
    // }
    RouteHelper.saveLastLocation(to)
    next()
  },

  beforeRouteUpdate(this: BaseWidgetLayout, to, from, next) {
    RouteHelper.saveWidgetLocation(to)
    RouteHelper.saveLastLocation(to)
    next()
  },
})
export class BaseWidgetLayout extends Vue {}
