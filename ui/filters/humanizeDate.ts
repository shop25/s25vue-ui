import { DateHelper } from '../helpers'

export default (timestamp: number): string => {
  if (!timestamp) {
    return ''
  }

  return DateHelper.format(timestamp)
}
