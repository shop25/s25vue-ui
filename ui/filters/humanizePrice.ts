import { Formatter } from '../helpers'

export default (value: number, symbol: string = '₽'): string => {
  if (!value) {
    return ''
  }

  return `${Formatter.prettyNumber(value)} ${symbol}`
}
