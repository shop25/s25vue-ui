import { UserService } from '../services'
import { HasInitials } from '@/types/User'

export default (user: HasInitials): string => {
  if (!user) {
    return ''
  }

  return UserService.getUserFullName(user)
}
