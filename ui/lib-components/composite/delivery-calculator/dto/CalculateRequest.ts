import { Dimensions } from './Dimensions'

interface Product {
  weight: number | null
  price: number | null
  dimensions: Dimensions
}
export interface LocationCodes {
  country: string | null
  city: string | null
}

export interface CalculateRequest {
  source: LocationCodes
  destination: LocationCodes
  products: Product[]
  currency: string | null
}
