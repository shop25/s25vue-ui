export default interface Region {
  id: number
  name: string
  code: string
}
