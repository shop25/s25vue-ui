export default interface Place {
  id: number
  code: string
  name: string
  district: string
  region: string
  type: string
  byZip: boolean
}
