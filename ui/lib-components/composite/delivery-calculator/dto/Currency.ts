export default interface Currency {
  default_fraction_digits: number
  display_name: string
  numeric_code: number
  prefix: boolean
  sub_unit: number
  symbol: string
  code: string
}
