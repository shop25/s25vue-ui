import Place from './Place'
import Country from './Country'
import Region from './Region'

export default interface ShipmentPoint {
  id: number
  city: Place
  country: Country
  state: Region
  enabled: boolean
}
