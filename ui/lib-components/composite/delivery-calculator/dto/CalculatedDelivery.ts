export default interface CalculatedDelivery {
  currency: string
  name: string
  price: number
  basePrice: number
  terms: { min: number; max: number }
  weightType: CalculatedDeliveryWeightType
  weight: number
}

type CalculatedDeliveryWeightType = 'natural' | 'volume'
