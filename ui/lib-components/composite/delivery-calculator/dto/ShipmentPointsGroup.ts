import ShipmentPoint from './ShipmentPoint'

export default interface ShipmentPointsGroup {
  countryId: number
  countryName: string
  points: ShipmentPoint[]
}
