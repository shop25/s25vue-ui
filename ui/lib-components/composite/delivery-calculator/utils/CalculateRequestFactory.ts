import { LocationCodes, CalculateRequest } from '../dto/CalculateRequest'
import { Dimensions } from '../dto/Dimensions'

export default function CalculateRequestFactory({
  source,
  destination,
  weight,
  dimensions,
  price,
  currency,
}: {
  source: LocationCodes
  destination: LocationCodes
  weight: number
  price: number
  dimensions: Dimensions
  currency: string | null
}): CalculateRequest {
  return {
    source: source,
    destination: destination,
    products: [{ weight, price, dimensions }],
    currency: currency,
  }
}
