import axios from 'axios'
import ShipmentPoint from '../dto/ShipmentPoint'

class ShipmentPointService {
  getShipmentPoints(): Promise<ShipmentPoint[]> {
    return axios
      .get('https://postcalc.s25.work/api/v1.0/shipmentPoints')
      .then(response => response.data)
  }
}

export default new ShipmentPointService()
