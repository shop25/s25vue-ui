import Currency from '../dto/Currency'

class CurrencyService {
  currencies: { [key: string]: Currency } = {
    RUB: {
      display_name: 'Рубль',
      numeric_code: 2,
      default_fraction_digits: 2,
      sub_unit: 100,
      symbol: '₽',
      prefix: false,
      code: 'RUB',
    },
    JPY: {
      display_name: 'Йена',
      numeric_code: 1,
      default_fraction_digits: 0,
      sub_unit: 1,
      symbol: '¥',
      prefix: true,
      code: 'JPY',
    },
    USD: {
      display_name: 'Доллар США',
      numeric_code: 3,
      default_fraction_digits: 2,
      sub_unit: 100,
      symbol: '$',
      prefix: false,
      code: 'USD',
    },
    EUR: {
      display_name: 'Евро',
      numeric_code: 5,
      default_fraction_digits: 2,
      sub_unit: 100,
      symbol: '€',
      prefix: false,
      code: 'EUR',
    },
  }

  getDefaultCurrency(): Currency {
    return this.currencies.RUB
  }
}

export default new CurrencyService()
