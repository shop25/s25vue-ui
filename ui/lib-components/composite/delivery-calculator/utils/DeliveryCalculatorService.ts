import axios from 'axios'
import { CalculateRequest } from '../dto/CalculateRequest'
import CalculatedDelivery from '../dto/CalculatedDelivery'

const DeliveryCalculatorService = {
  calculateDelivery(request: CalculateRequest): Promise<CalculatedDelivery[]> {
    axios.defaults.withCredentials = true
    return axios
      .post('https://postcalc.s25.work/api/v1.0/calculate', request, {
        withCredentials: false,
      })
      .then(
        response =>
          Object.values(response.data.deliveries || {}).filter(
            delivery => delivery
          ) as CalculatedDelivery[]
      )
  },

  calculateDeliveryPoints(
    request: CalculateRequest
  ): Promise<CalculatedDelivery[]> {
    axios.defaults.withCredentials = true
    return axios
      .post(
        'https://postcalc.s25.work/api/v1.0/calculateDeliveryPoints',
        request,
        {
          withCredentials: false,
        }
      )
      .then(
        response =>
          Object.values(response.data.deliveries || {}).filter(
            delivery => delivery
          ) as CalculatedDelivery[]
      )
  },
}

export default DeliveryCalculatorService
