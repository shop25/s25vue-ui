import axios from 'axios'
import { Track } from '../dto/Track'
import { Carrier } from '../dto/Carrier'

export interface getTrackPayload {
  trackNumber: string
  carrierCode: string
  projectHost: string
}

class DeliveryTrackingService {
  host = ''

  init(config: { host: string }) {
    this.host = config.host
  }

  getCarriers(): Promise<Carrier[]> {
    return axios
      .get(`${this.host}/api/carriers/list`, {
        withCredentials: false,
      })
      .then(response => response.data)
  }

  getTrack(payload: getTrackPayload): Promise<Track> {
    return axios
      .get(`${this.host}/api/track`, {
        params: {
          number: payload.trackNumber,
          service: payload.projectHost,
          carrier: payload.carrierCode,
        },
        withCredentials: false,
      })
      .then(response => response.data.track)
  }
}

export default new DeliveryTrackingService()
