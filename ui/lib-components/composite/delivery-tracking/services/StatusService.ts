import { TrackStatus } from '../dto/TrackStatus'

class StatusService {
  public statuses: { [key in TrackStatus]: TrackStatus } = {
    InTransit: 'InTransit',
    Delivered: 'Delivered',
    NotFound: 'NotFound',
    Return: 'Return',
    Error: 'Error',
    New: 'New',
    Expired: 'Expired',
    PickUp: 'PickUp',
  }

  public statusLabels: { [key in TrackStatus]: string } = {
    InTransit: 'В пути',
    Delivered: 'Доставлен',
    NotFound: 'Не найден',
    Return: 'Возврат',
    Error: 'Ошибка',
    New: 'Новый',
    Expired: 'Устарел',
    PickUp: 'Прибыл',
  }

  getStatusLabel(status: TrackStatus): string {
    return this.statusLabels[status]
  }
}

export default new StatusService()
