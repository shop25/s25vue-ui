export type TrackStatus =
  | 'InTransit'
  | 'Delivered'
  | 'NotFound'
  | 'Return'
  | 'Error'
  | 'New'
  | 'Expired'
  | 'PickUp'
