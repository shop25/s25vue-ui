export interface TrackEvent {
  id: number
  countryName: string
  courierName: string
  operation: string
  placeName: string
  date: number
}
