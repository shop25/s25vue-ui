export interface Carrier {
  id: number
  alias: string
  name: string
  uid: string
  trackingUrl: string | null
}
