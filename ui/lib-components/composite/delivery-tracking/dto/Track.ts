import { Carrier } from './Carrier'
import { TrackEvent } from './TrackEvent'

export interface Track {
  errorsCount: number
  hasError: boolean
  id: number
  number: string
  service: string
  status: string
  createdAt: number
  updatedAt: number
  closedAt: number | null
  lastProcessedAt: number
  inTransit: number
  carrier: Carrier
  events: TrackEvent[]
}
