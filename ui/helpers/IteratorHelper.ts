class IteratorHelper {
  filter<T>(cb: (item: T) => boolean) {
    return function*(items: Iterable<T>) {
      for (const item of items) {
        if (cb(item)) {
          yield item
        }
      }
    }
  }

  map<T, R>(cb: (item: T) => R) {
    return function*(items: Iterable<T>) {
      for (const item of items) {
        yield cb(item)
      }
    }
  }

  take<T>(count: number) {
    return function*(items: Iterable<T>) {
      const iterator = items[Symbol.iterator]()

      for (let i = 0; i < count; i++) {
        const next = iterator.next()
        if (next.done) {
          break
        }

        yield next.value
      }
    }
  }
}

export default new IteratorHelper()
