export default function compose<I, O>(onlyFn: (x: I) => O): (x: I) => O
export default function compose<I, M, O>(
  firstFn: (x: I) => M,
  lastFn: (x: M) => O
): (x: I) => O
export default function compose<I, M1, M2, O>(
  firstFn: (x: I) => M1,
  fn1: (x: M1) => M2,
  lastFn: (x: M2) => O
): (x: I) => O
export default function compose<I, M1, M2, M3, O>(
  firstFn: (x: I) => M1,
  fn1: (x: M1) => M2,
  fn2: (x: M2) => M3,
  lastFn: (x: M3) => O
): (x: I) => O
export default function compose<I, M1, M2, M3, M4, O>(
  firstFn: (x: I) => M1,
  fn1: (x: M1) => M2,
  fn2: (x: M2) => M3,
  fn3: (x: M3) => M4,
  lastFn: (x: M4) => O
): (x: I) => O
export default function compose<I, M1, M2, M3, M4, M5, O>(
  firstFn: (x: I) => M1,
  fn1: (x: M1) => M2,
  fn2: (x: M2) => M3,
  fn3: (x: M3) => M4,
  fn4: (x: M4) => M5,
  lastFn: (x: M5) => O
): (x: I) => O
// Add as many overload signature as you need for your purposes
export default function compose<Fns extends ((x: unknown) => never)[]>(
  ...fns: Fns
): (x: unknown) => never {
  return (x: unknown) => {
    for (const fn of fns) {
      x = fn(x)
    }
    return x as never
  }
}
