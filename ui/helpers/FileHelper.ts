class FileHelper {
  fileToBase64(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()

      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result as string)
      reader.onerror = error => reject(error)
    })
  }

  isMediaByMimeType(mimetype: string): boolean {
    return ['image', 'video'].some(
      type => mimetype.includes(type) && !mimetype.includes('tiff')
    )
  }

  isImageByMimeType(mimetype: string): boolean {
    return mimetype.includes('image') && !mimetype.includes('tiff')
  }

  isVideoByMimeType(mimetype: string): boolean {
    return mimetype.includes('video')
  }
}

export default new FileHelper()
