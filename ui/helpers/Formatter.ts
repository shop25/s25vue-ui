class Formatter {
  // 1000000 -> 1 000 000
  prettyNumber(value: number) {
    return value.toLocaleString('ru-RU')
  }

  // 1000 -> 1K
  prettyCount(value: number): string {
    if (value < 1000) {
      return value.toString()
    }

    return value < 1000_000
      ? (value / 1000).toFixed(1) + 'K'
      : (value / 1000_000).toFixed(1) + 'M'
  }

  prettyPrice(price: number, currency: string) {
    const options = { style: 'currency', currency }

    return new Intl.NumberFormat('ru-RU', options).format(price)
  }

  num2str(number: number, one: string, two: string, five: string) {
    number = Math.abs(number) % 100
    let n1 = number % 10

    if (number > 10 && number < 20) {
      return five
    }

    if (n1 > 1 && n1 < 5) {
      return two
    }

    if (n1 == 1) {
      return one
    }

    return five
  }
}

export default new Formatter()
