import { Notify } from 'quasar'

Notify.setDefaults({
  position: 'bottom-right',
  message: '',
  color: 'blue-grey',
  textColor: 'white',
  timeout: 10000,
  classes: 'shadow-2',
})

const NotifyHelper = {
  notify: Notify,
  success(message: string, config: any = {}) {
    Notify.create({ message: `👍 ${message}`, ...config })
  },
  error(message: string, config: any = {}) {
    Notify.create({
      message: `☠️ ${message}`,
      ...{ textColor: 'white', color: 'negative' },
      ...config,
    })
  },
  serverError() {
    this.error('Ошибка сервера')
  },
}

export default NotifyHelper
