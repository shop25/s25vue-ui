import axios from 'axios'

axios.defaults.withCredentials = true

export { default as DynamicModuleFactory } from './DynamicModuleFactory'
export { default as BaseModule } from './BaseModule'
export { default as DeliveryCalculatorStore } from '../lib-components/composite/delivery-calculator/utils/DeliveryCalculatorStore'
export {
  default as DeliveryTrackingStore,
  getDeliveryTrackingStore,
} from '../lib-components/composite/delivery-tracking/store/DeliveryTrackingStore'
