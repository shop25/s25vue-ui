import { Mutation, VuexModule } from 'vuex-module-decorators'
import { User } from '@/types'
import { Manager } from '@/types/Manager'

type CurrentUser = User | Manager | null

export class BaseUserModule extends VuexModule {
  isLoading = false
  user: CurrentUser = null

  @Mutation
  setUser(user: CurrentUser) {
    this.user = user
  }

  @Mutation
  setLoading(value: boolean) {
    this.isLoading = value
  }
}
