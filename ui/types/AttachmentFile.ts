export type AttachmentFile = {
  id: number
  filename: string
  filesize: number
  filetype: string
  extension: string
  url: string
}
