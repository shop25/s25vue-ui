type ResponseErrors = { [key: string]: string }

export default ResponseErrors
