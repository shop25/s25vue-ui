export default interface User extends HasInitials {
  id: string
  positionId: number | null
  avatar: string | null
  groupId: number | null
  timezoneId: number | null
  authMethodId: number
  phone: string
  password: string
  personnelNumber: string
  enabled: boolean
  createdAt: string
  positionName: string
}

export interface HasInitials {
  firstName: string
  lastName: string
}
