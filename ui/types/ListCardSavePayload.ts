type ListCardSavePayload<ItemType> = {
  title: string
  items: ItemType[]
}

export default ListCardSavePayload
