import { HasInitials } from './User'

export interface Manager extends HasInitials {
  id: string
  position: string
  avatar: string | null
  avatar2x: string | null
  enabled: boolean
}

export interface ManagerCollection {
  [key: string]: Manager
}

export interface ManagerCollectionsByGroup {
  [key: string]: ManagerCollection
}
