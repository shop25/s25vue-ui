import { IteratorHelper, compose } from '@/helpers'
import JSZip from 'jszip'
import Papa, { ParseResult, ParseConfig } from 'papaparse'

type CsvRecord = { [key: string]: string }

type CsvPreviewValidator<T> = (value: Required<T>[keyof T], row: T) => boolean

interface CsvPreviewOptions<T> {
  // Поля, отсутсвие которых в заголовке приведет к исключению
  requiredFields?: (keyof T)[]
  // Как много строк нужно распарсить в файле/строке
  rowsLimit?: number
  // Валидаторы столбцов
  validators?: { [F in keyof T]?: CsvPreviewValidator<T> }
  fieldAliases?: { [key: string]: keyof T }
}

export default class CsvPreviewService<T> {
  constructor(private readonly options: CsvPreviewOptions<T> = {}) {}

  async execute(
    file: File,
    options?: CsvPreviewOptions<T>
  ): Promise<CsvPreviewResult> {
    const worker = new CsvPreviewWorker<T>(
      Object.assign({}, this.options, options)
    )

    return worker.execute(file)
  }
}

class CsvPreviewWorker<T> {
  private readonly options: CsvPreviewOptions<T>
  private fieldAliases?: Map<keyof T, string[]>

  constructor(options: CsvPreviewOptions<T> = {}) {
    this.options = options
  }

  private getFieldAliases(field: keyof T): string[] {
    this.fieldAliases =
      this.fieldAliases ??
      Object.entries(this.options.fieldAliases ?? {}).reduce(
        (acc, [alias, field]) => {
          if (!acc.has(field)) {
            acc.set(field, [])
          }
          acc.get(field)!.push(alias)
          return acc
        },
        new Map<keyof T, string[]>()
      )

    return this.fieldAliases.get(field)!
  }

  public async execute(file: File): Promise<CsvPreviewResult> {
    const unzipped = await this.unzip(file)

    const result = await this.parse(unzipped)

    const fieldsError = this.validateFields(result)

    if (fieldsError) {
      return new CsvPreviewResult([], result.meta.delimiter, [], fieldsError)
    }

    const rows = this.normalizeRows(result.data)

    const errors = this.validateRows(rows)

    const { delimiter } = result.meta

    return new CsvPreviewResult(rows, delimiter, errors)
  }

  private async unzip(file: File): Promise<File | string> {
    if (file.type !== 'application/zip') {
      return file
    }

    let zip = null
    try {
      zip = await JSZip.loadAsync(file)
    } catch (e) {
      throw new Error('Файл не является ZIP-архивом или поврежден')
    }

    const zipFile = Object.values(zip.files)?.[0]

    if (!zipFile) {
      throw new Error('Архив пуст')
    }

    return await zipFile.async('string')
  }

  private parse(file: File | string) {
    const config: ParseConfig = {
      header: true,
      preview: this.options.rowsLimit ?? 0,
      worker: true,
      skipEmptyLines: true,
    }

    return new Promise<ParseResult<CsvRecord>>((resolve, reject) => {
      Papa.parse<CsvRecord>(file, {
        ...config,
        complete(results) {
          resolve(results)
        },
        error(error) {
          reject(error)
        },
      })
    })
  }

  private validateFields(result: ParseResult<CsvRecord>): string | undefined {
    const fields = result.meta.fields

    if (!fields) {
      return 'Не распарсились заголовки'
    }

    const { requiredFields } = this.options

    if (!requiredFields || requiredFields.length === 0) {
      return
    }

    const missedFields: string[] = []

    for (const requiredField of requiredFields) {
      const requiredAliases = this.getFieldAliases(requiredField)

      if (
        fields.includes(requiredField as string) ||
        requiredAliases.some(a => fields.includes(a))
      ) {
        continue
      }

      const akas =
        requiredAliases.length > 0
          ? ` (a.k.a. ${requiredAliases.join(', ')})`
          : ''

      missedFields.push(`${String(requiredField)}${akas}`)
    }

    if (missedFields.length) {
      return `Отсутствуют обязательные поля: ${missedFields.join(', ')}`
    }
  }

  private normalizeRows(data: CsvRecord[]): CsvRecord[] {
    const { fieldAliases = {} } = this.options

    return data.map(row => {
      const normalRow: CsvRecord = {}
      for (const [key, value] of Object.entries(row)) {
        const unaliasedKey = fieldAliases[key] ?? key
        normalRow[unaliasedKey as string] = value
      }
      return normalRow
    })
  }

  private validateRows(rows: CsvRecord[]): number[] {
    const { validators } = this.options

    const errors: number[] = []

    if (!validators) {
      return errors
    }

    for (let i = 0, len = rows.length; i < len; i++) {
      const row = rows[i]

      for (const [field, func] of Object.entries(validators) as [
        string,
        CsvPreviewValidator<CsvRecord>
      ][]) {
        const val = row[field]

        if (!func(val, row)) {
          errors.push(i)

          break
        }
      }
    }

    return errors
  }
}

class CsvPreviewResult {
  constructor(
    public rows: CsvRecord[],
    public delimiter: string,
    public errors: number[],
    public fieldsError?: string
  ) {}

  takeValidRows(count: number): CsvRecord[] {
    const { rows, errors } = this

    if (errors.length === 0) {
      return rows.slice(0, count)
    }

    const fn = compose(
      IteratorHelper.filter(([index]: [number, CsvRecord]) =>
        errors.includes(index)
      ),
      IteratorHelper.take(count),
      IteratorHelper.map(([, row]: [number, CsvRecord]) => row)
    )

    return [...fn(rows.entries())]
  }

  takeInvalidRows(count: number): CsvRecord[] {
    const { rows, errors } = this

    const result: CsvRecord[] = []
    for (const error of errors.slice(0, count)) {
      result.push(rows[error])
    }

    return result
  }
}
