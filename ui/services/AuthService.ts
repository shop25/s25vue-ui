import axios from 'axios'

const instance = axios.create()

export class BaseAuthService {
  baseUrl = ''
  promise: Promise<string> | null = null

  getAuthToken(): Promise<string> {
    if (!this.promise) {
      this.promise = instance
        .get(`${this.baseUrl}/get_auth_token`, { withCredentials: true })
        .then(response => response.data.authToken)
    }

    return this.promise
  }
}
