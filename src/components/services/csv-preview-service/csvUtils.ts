export function nonEmptyString(val: unknown) {
  return typeof val === 'string' && val.trim().length > 0
}

export function positiveNumber(val: unknown) {
  return (
    typeof val === 'number' || (typeof val === 'string' && parseFloat(val) > 0)
  )
}

export function brandName(brandName: string) {
  const reText = brandName
    .split(/[\s-]+/)
    .map(x => x.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&'))
    .join('[\\s-]+')

  const re = new RegExp(reText, 'i')

  return (val: unknown) => typeof val === 'string' && re.test(val)
}

export function exactMatch(sample: unknown) {
  return (val: unknown) => val === sample
}

export function optional<T extends unknown>(
  validator: (val: unknown) => boolean
) {
  return (val: T) => {
    return (
      val === null || typeof val === 'undefined' || val === '' || validator(val)
    )
  }
}
