import { store } from 'quasar/wrappers'
import Vuex from 'vuex'
import Vue from 'vue'
import DeliveryCalculatorStore from '@/lib-components/composite/delivery-calculator/utils/DeliveryCalculatorStore'
import DeliveryTrackingStore from '@/lib-components/composite/delivery-tracking/store/DeliveryTrackingStore'
import { MessagesModules, NotificationsModules, TasksModules } from '@/widgets'
import { HashNavigationModule } from '@/entry'

Vue.use(Vuex)

export const rootStore = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    DeliveryCalculatorStore,
    DeliveryTrackingStore,
    HashNavigationModule,
    ...MessagesModules,
    ...TasksModules,
    ...NotificationsModules,
  },
})

export default store(function() {
  return rootStore
})
