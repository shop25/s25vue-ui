import { VueConstructor } from 'vue'

export interface LoadedComponentExample {
  component: VueConstructor
  rawComponent: string
}

class ExampleLoader {
  contexts = {
    components: {
      context: require.context('components/examples', true, /\.vue/),
      contextRaw: require.context(
        '!!raw-loader!components/examples',
        true,
        /\.vue/
      ),
    },
    services: {
      context: require.context('components/services', true, /\.vue/),
      contextRaw: require.context(
        '!!raw-loader!components/services',
        true,
        /\.vue/
      ),
    },
    widgets: {
      context: require.context('components/widgets', true, /\.vue/),
      contextRaw: require.context(
        '!!raw-loader!components/widgets',
        true,
        /\.vue/
      ),
    },
  }

  getExamples(
    section: 'components' | 'services' | 'widgets',
    componentName: string
  ): LoadedComponentExample[] {
    const { context, contextRaw } = this.contexts[section]

    return context
      .keys()
      .map(id => {
        const pathArray = id.split('/')
        const componentSlug = pathArray[pathArray.length - 2]

        if (componentSlug === componentName) {
          const component = context(id).default
          const rawComponent = contextRaw(id).default

          return {
            component: component as VueConstructor,
            rawComponent: String(rawComponent),
          }
        }

        return null
      })
      .filter(example => example !== null) as LoadedComponentExample[]
  }
}

const exampleLoader = new ExampleLoader()

export default exampleLoader
