import { route } from 'quasar/wrappers'
import VueRouter from 'vue-router'
import routes from './routes'
import { hashNavigationHookFactory, messagesHashNavRoutes } from '@/widgets'
import { rootStore } from 'src/store'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default route(function({ Vue }) {
  Vue.use(VueRouter)

  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  })

  const hook = hashNavigationHookFactory(
    rootStore,
    Object.values(messagesHashNavRoutes)
  )

  Router.beforeEach(hook)

  return Router
})
