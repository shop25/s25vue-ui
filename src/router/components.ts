import { PageInfo } from 'src/router/PageInfo'

export const components: PageInfo[] = [
  {
    title: 'Toolbar',
    path: '/components/toolbar',
    component: 'toolbar',
  },
  {
    title: 'Tabs',
    path: '/components/tabs/common',
    group: 'tabs',
    children: [
      {
        title: 'Tabs',
        path: '/components/tabs/tabs',
        component: 'tabs',
      },
      {
        title: 'Tab panels',
        path: '/components/tabs/tab-panels',
        component: 'tab-panels',
      },
    ],
  },
  {
    title: 'Page',
    path: '/components/page/common',
    group: 'page',
    children: [
      {
        title: 'Page',
        path: '/components/page/page',
        component: 'page',
      },
      {
        title: 'Page splitter',
        path: '/components/page/page-splitter',
        component: 'page-splitter',
      },
      {
        title: 'Page header',
        path: '/components/page/page-header',
        component: 'page-header',
      },
      {
        title: 'Page loading',
        path: '/components/page/page-loading',
        component: 'page-loading',
      },
    ],
  },
  {
    title: 'CSV',
    path: '/components/csv/csv',
    group: 'csv',
    children: [
      {
        title: 'Csv select file',
        path: '/components/csv/csv-select-file',
        component: 'csv-select-file',
      },
      {
        title: 'Csv import',
        path: '/components/csv/csv-import',
        component: 'csv-import',
      },
      {
        title: 'Csv import process',
        path: '/components/csv/csv-import-process',
        component: 'csv-import-process',
      },
      {
        title: 'Csv import info',
        path: '/components/csv/csv-import-info',
        component: 'csv-import-info',
      },
    ],
  },
  {
    title: 'Datalist',
    path: '/components/datalist',
    component: 'datalist',
  },
  {
    title: 'Card',
    path: '/components/card',
    component: 'card',
  },
  {
    title: 'Log',
    path: '/components/log',
    component: 'log',
  },
  {
    title: 'Chips & Badges',
    path: '/components/chip-badges/chip-badges',
    group: 'chip-badges',
    children: [
      {
        title: 'Chip',
        path: '/components/chip-badges/chip',
        component: 'chip',
      },
      {
        title: 'Badge',
        path: '/components/chip-badges/badge',
        component: 'badge',
      },
    ],
  },
  {
    title: 'Composite blocks',
    path: '/components/composite-blocks/composite-blocks',
    group: 'composite-blocks',
    children: [
      {
        title: 'Adding panel',
        path: '/components/composite-blocks/adding-panel',
        component: 'adding-panel',
      },
      {
        title: 'Form controls',
        path: '/components/composite-blocks/form-controls',
        component: 'form-controls',
      },
      {
        title: 'Loading panel',
        path: '/components/composite-blocks/loading-panel',
        component: 'loading-panel',
      },
      {
        title: 'Delivery calculator',
        path: '/components/composite-blocks/delivery-calculator',
        component: 'delivery-calculator',
      },
      {
        title: 'Delivery tracking',
        path: '/components/composite-blocks/delivery-tracking',
        component: 'delivery-tracking',
      },
      {
        title: 'List Card',
        path: '/components/composite-blocks/list-card',
        component: 'list-card',
      },
    ],
  },
  {
    title: 'Dialog',
    path: '/components/dialog',
    component: 'dialog',
  },
  {
    title: 'Drawer',
    path: '/components/drawer',
    component: 'drawer',
  },
  {
    title: 'Menu',
    path: '/components/menu',
    component: 'menu',
  },
  {
    title: 'User avatar',
    path: '/components/user-avatar',
    component: 'user-avatar',
  },
  {
    title: 'User menu',
    path: '/components/user-menu',
    component: 'user-menu',
  },
  {
    title: 'Zebra',
    path: '/components/zebra',
    component: 'zebra',
  },
  {
    title: 'Flash',
    path: '/components/flash',
    component: 'flash',
  },
  {
    title: 'List',
    path: '/components/list',
    component: 'list',
  },
  {
    title: 'Form',
    path: '/components/form/form',
    group: 'form',
    children: [
      {
        title: 'Input',
        path: '/components/form/input',
        component: 'input',
      },
      {
        title: 'Phone input',
        path: '/components/form/phone-input',
        component: 'phone-input',
      },
      {
        title: 'Select',
        path: '/components/form/select',
        component: 'select',
      },
      {
        title: 'Autocomplete',
        path: '/components/form/autocomplete',
        component: 'autocomplete',
      },
      {
        title: 'Option group',
        path: '/components/form/option-group',
        component: 'option-group',
      },
      {
        title: 'Toggle',
        path: '/components/form/toggle',
        component: 'toggle',
      },
      {
        title: 'Toggle group',
        path: '/components/form/toggle-group',
        component: 'toggle-group',
      },
      {
        title: 'Btn toggle',
        path: '/components/form/btn-toggle',
        component: 'btn-toggle',
      },
      {
        title: 'File input btn',
        path: '/components/form/file-input-btn',
        component: 'file-input-btn',
      },
      {
        title: 'Search input',
        path: '/components/form/search-input',
        component: 'search-input',
      },
      {
        title: 'Search select',
        path: '/components/form/search-select',
        component: 'search-select',
      },
    ],
  },
  {
    title: 'Buttons',
    path: '/components/buttons/buttons',
    group: 'buttons',
    children: [
      {
        title: 'Btn',
        path: '/components/buttons/btn',
        component: 'btn',
      },
      {
        title: 'Btn icon',
        path: '/components/buttons/btn-icon',
        component: 'btn-icon',
      },
      {
        title: 'Btn add',
        path: '/components/buttons/btn-add',
        component: 'btn-add',
      },
      {
        title: 'Btn close',
        path: '/components/buttons/btn-close',
        component: 'btn-close',
      },
      {
        title: 'Btn save',
        path: '/components/buttons/btn-save',
        component: 'btn-save',
      },
      {
        title: 'Btn cancel',
        path: '/components/buttons/btn-cancel',
        component: 'btn-cancel',
      },
      {
        title: 'Btn menu',
        path: '/components/buttons/btn-menu',
        component: 'btn-menu',
      },
    ],
  },
  {
    title: 'Bar',
    path: '/components/bar-n-buttons/bar-n-buttons',
    group: 'bar-n-buttons',
    children: [
      {
        title: 'Bar',
        path: '/components/bar-n-buttons/bar',
        component: 'bar',
      },
      {
        title: 'Bar Btn',
        path: '/components/bar-n-buttons/bar-btn',
        component: 'bar-btn',
      },
      {
        title: 'Bar Btn Cancel',
        path: '/components/bar-n-buttons/bar-btn-cancel',
        component: 'bar-btn-cancel',
      },
      {
        title: 'Bar Btn Save',
        path: '/components/bar-n-buttons/bar-btn-save',
        component: 'bar-btn-save',
      },
      {
        title: 'Bar Btn Toggle',
        path: '/components/bar-n-buttons/bar-btn-toggle',
        component: 'bar-btn-toggle',
      },
    ],
  },
  {
    title: 'Progress',
    path: '/components/progress/progress',
    group: 'progress',
    children: [
      {
        title: 'Linear progress',
        path: '/components/progress/linear-progress',
        component: 'linear-progress',
      },
      {
        title: 'Circular progress',
        path: '/components/progress/circular-progress',
        component: 'circular-progress',
      },
    ],
  },
]
