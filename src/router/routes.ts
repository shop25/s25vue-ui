import { RouteConfig } from 'vue-router'
import ComponentPage from 'pages/ComponentPage.vue'
import ComponentsPage from 'pages/ComponentsPage.vue'
import InstallPage from 'pages/InstallPage.vue'
import StorePage from 'pages/StorePage.vue'
import ServicesPage from 'pages/ServicesPage.vue'
import { Routes as TaskRoutes } from '@/widgets/tasks/router/routes'
import { Routes as MessageRoutes } from '@/widgets/messages/router/routes'
import { Routes as NotificationRoutes } from '@/widgets/notifications/router/routes'
import WidgetsPage from 'pages/WidgetsPage.vue'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    redirect: 'install',
    props: true,
    children: [
      {
        path: 'install',
        name: 'install',
        component: InstallPage,
      },
      {
        path: 'store',
        name: 'store',
        component: StorePage,
      },
      {
        path: 'components',
        name: 'components',
        component: ComponentsPage,
        children: [
          {
            path: ':componentName',
            component: ComponentPage,
            props: ({ params }) => ({ section: 'components', ...params }),
          },
          {
            path: ':groupName/:componentName',
            component: ComponentPage,
            props: ({ params }) => ({ section: 'components', ...params }),
          },
        ],
      },
      {
        path: 'services',
        name: 'services',
        component: ServicesPage,
        children: [
          {
            path: ':componentName',
            component: ComponentPage,
            props: ({ params }) => ({ section: 'services', ...params }),
          },
          {
            path: ':groupName/:componentName',
            component: ComponentPage,
            props: ({ params }) => ({ section: 'services', ...params }),
          },
        ],
      },
      {
        path: '',
        name: 'widgets',
        component: WidgetsPage,
        children: [...TaskRoutes, ...MessageRoutes, ...NotificationRoutes],
      },
    ],
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  })
}

export default routes
