import { PageInfo } from 'src/router/PageInfo'

export const services: PageInfo[] = [
  {
    title: 'CsvPreviewService',
    path: '/services/csv-preview-service',
    component: 'csv-preview-service',
  },
]
