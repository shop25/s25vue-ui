export interface PageInfo {
  title: string
  path: string
  component?: string
  group?: string
  children?: PageInfo[]
}
