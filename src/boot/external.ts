import { boot } from 'quasar/wrappers'
import PortalVue from 'portal-vue'
import axios from 'axios'
import humanizePrice from '@/filters/humanizePrice'
import humanizeDate from '@/filters/humanizeDate'
import userFullName from '@/filters/userFullName'
import { Component } from 'vue-property-decorator'

export default boot(({ Vue }) => {
  Component.registerHooks([
    'beforeRouteEnter',
    'beforeRouteLeave',
    'beforeRouteUpdate',
  ])
  Vue.use(PortalVue)
  Vue.filter('humanizePrice', humanizePrice)
  Vue.filter('humanizeDate', humanizeDate)
  Vue.filter('userFullName', userFullName)

  axios.defaults.withCredentials = true
})
